# Windows 10 Insider Preview Skip Ahead Build 17704.1000

[![N|Solid](https://upload.wikimedia.org/wikipedia/commons/c/c7/Windows_logo_-_2012.png)](https://upload.wikimedia.org/wikipedia/commons/c/c7/Windows_logo_-_2012.png)

This version of windows contains the following files:

  - amd64_en-us_17704.1000.iso
  - x86_en-us_17704.1000.iso

# New Features

  - Microsoft Edge Improvements
    - New Microsoft Edge Beta logo
    - New design improvements
    - Redesigned “…” menu and Settings
    - Customize toolbar items for Microsoft Edge
    - Control whether media can play automatically
    - New PDF icon
  - Skype for Windows 10 gets a big update!
    - Best in class calling experience
    - Flexible group call canvas
    - Take snapshots
    - Easily start screensharing
    - New layout
    - Customizable themes
    - And much more
  - New Diagnostic Data Viewer features to improve your Privacy Experience
    - Common Data like the OS name, version, device ID, Device Class, and diagnostic level selection
    - Device Connectivity and Configuration such as device properties, preferences, settings, and network information
    - Product and Service Performance such as device health, performance and reliability, and device file queries (this is not meant to capture user patterns or habits)
    - Browsing History such as frequently visited sites
    - Product and Service Usage data like applications and services used
    - Software Setup and Inventory such as installed apps and device update information.
#### And much more!!! Please visit https://blogs.windows.com/windowsexperience/2018/06/27/announcing-windows-10-insider-preview-build-17704/ for more information!